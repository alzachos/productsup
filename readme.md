# My Solution
The project is upload in bitbucket repo https://alzachos@bitbucket.org/alzachos/productsup.git

### Jenkinsfile
Orchestrates the clone from SCM, build of server using docker compose, build of the project, check the json response, cleanup

### task/docker-compose.yaml
A docker compose file that creates nginx, mariadb and php-fpm services

### task/php-fpm.dockerfile
A dockerfile that installs packages that are necessary for the project. It is used by the docker-compose file on build.

### task/site.conf
Nginx configuration file. Link to php service is used.

## Changes in project files
### project/index.php
Instead of localhost the name of mariadb service is used