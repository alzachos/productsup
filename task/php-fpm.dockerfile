FROM php:7.4-fpm
RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
RUN sed -i -e "s/^ *memory_limit.*/memory_limit = 1G/g" -e "s/^ *max_execution_time.*/max_execution_time = 45/g" /usr/local/etc/php/php.ini
RUN apt update && apt install -y zip
RUN docker-php-ext-install pdo pdo_mysql
# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
