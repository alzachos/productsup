# DevOps Applicant Test

## Preface

The purpose of this test is to evaluate the knowledge in solving a problem within a specific timeframe.
The actual outcome of the test is not as relevant as the method you got to resolving it.

Using Google and/or Stackoverflow to find the answer or result is fine, because real world scenarios will allow for this too.

In case you have questions or are really stuck, I can be reached on gl@productsup.com

## Goal

The entire project needs to be delivered in the form of a `Docker-Compose` file and a `Jenkinsfile`

Inside the `/project` directory you'll find a web application that needs to be deployed on the server provided to you for the test.
Inside the `/data` directory you'll find the SQL dump required for the application to run.


## General Specifications

The project has the following requirements to run on a server:

- nginx
  - version 1.14.0 or greater 
- php-fpm
  - version 7.4 or greater
- mariadb
  - version 10.1 or greater

- The project has static files which need to be delivered with the correct `mime type`.
- PHP needs to be configured to have 1GB of memory available for the process. An execution time of 45seconds is required.

## Nginx Specification

Here is a sample file for nginx:

server {
    listen       8080;
    server_name  _;
    root   /home/project/public/;
    try_files $uri /index.php?$query_string;
    index index.php;

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    location ~ \.php$ {
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
	      fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include        fastcgi_params;
    }
}

## MariaDB Spec

There needs to be a user created for the project to be able to function properly.
The project expects the following:

| key       | value        |
| --------- | ------------ |
| host      | 127.0.0.1    |
| port      | 3306         |
| database  | foobar       |
| user      | foo_user     |
| password  | foo_pass     |
| root user | root         |
| root pass | correcthorse |

## Jenkinsfile specs
The jenkinsfile must check out the git repo, build the project in docker and check that the web server returns a valid JSON array, then it should clean up the build.
